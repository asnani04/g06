This is an assignment that we are doing as a part of the course CS251 - Computing Laboratory. This is supposed to make us learn Git and Bitbucket, as well as write short tutorials on the topics that we have covered throughout the semester, and finally present them as a LaTeX doc as well as a Beamer.

Group Members: (in alphabetical order)

1. Abhishek Gunda
2. Ankit Gupta
3. Kishan Gopal
4. Nishit Asnani 

file.tex is the source for the LaTeX file, and beamer.tex is the source for the Beamer.